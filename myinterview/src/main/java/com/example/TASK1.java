package com.example;

import java.util.ArrayList;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {

        private boolean isPalindrome;
        
        public void test(){
            ArrayList<Boolean> test = new ArrayList<Boolean>();
            test.add(isPalindrome("racecar"));
            test.add(isPalindrome("coconut"));
            test.add(isPalindrome("noon"));
            test.add(isPalindrome("boot"));
            
            System.out.print("--------------TASK1--------------\n");
            System.out.println("Input = racecar - " + test.get(0));
            System.out.println("Input = coconut - " + test.get(1));
            System.out.println("Input = noon - " + test.get(2));
            System.out.println("Input = boot - " + test.get(3));
        }
        
        public boolean isPalindrome(String in){
            int len = in.length();
            String first = in.substring(0,1);
            String last = in.substring(len-1, len);

            if(in.length()%2 == 0){
                
                //If string is even
                int max = (in.length()-1) / 2;

                for(int i=0; i<max; i++){
                    if(first.equals(last)){
                        isPalindrome = true;
                        first = in.substring(i, i+1);
                        last = in.substring(len-1-i, len-i);
                    } else{
                        isPalindrome = false;
                        break;
                    }
                }
            } else{
                
                //If string is odd           
                int max = in.length() / 2;
                
                for(int i=0; i<max; i++){
                    if(first.equals(last)){
                        isPalindrome = true;
                        first = in.substring(i, i+1);
                        last = in.substring(len-1-i, len-i);
                    } else{
                        isPalindrome = false;
                        break;
                    }
                }
                    
            }
            
            return isPalindrome;
        }
}
