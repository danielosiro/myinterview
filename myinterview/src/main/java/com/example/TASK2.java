package com.example;

import java.util.ArrayList;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */

class listItem{
    private final int index;
    private final String name, before;
    private String after;
    
    public listItem(int i, String n, String b){
        index = i;
        name = n;
        before = b;
        after = null;
    }
    
    void updateAfter(String a){
        after = a;
    }
    
    String getName(){
        return name;
    }
    String getBefore(){
        return before;
    }
    String getAfter(){
        return after;
    }
}

public class TASK2 {
    
    ArrayList<listItem> list = new ArrayList<listItem>();
    
    public void test(){
        System.out.print("--------------TASK2--------------\n");
        
        writeToList("test1");
        writeToList("test2");
        writeToList("test3");
        
        showList();
        
        System.out.print("\n");
        list.remove(1);
        
        showList(); 
    }
    
    public void writeToList(String name){
        
        listItem item;
        String before;
        
        if(list.size() > 0){
            
            int index = list.size();
            before = list.get(index-1).getName();
            
            //Add next item
            item = new listItem(index, name, before);
            list.add(item);
            
            //Update "after" of previous on the list
            list.get(index-1).updateAfter(name);
                 
        } else{
            
            //First item on list
            item = new listItem(0, name, null);         
            list.add(item);
        }
    }
    
    public void showList(){
        for(int i=0; i<list.size(); i++){
            String name = list.get(i).getName();
            System.out.println(name);
        }
    }

}
