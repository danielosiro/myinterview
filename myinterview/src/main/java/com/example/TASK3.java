package com.example;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

    ArrayList<String> arr;
    
    public void test(){
        arr = new ArrayList<String>();
        
        arr.add("test1");
        arr.add("test1");
        arr.add("test3");
        arr.add("test3");
        arr.add("test2");
        
        System.out.print("--------------TASK3--------------\n");
        
        System.out.print("Input string[1] = test1\n");
        System.out.print("Input string[1] = test1\n");
        System.out.print("Input string[1] = test3\n");
        System.out.print("Input string[1] = test3\n");
        System.out.print("Input string[1] = test2\n");
        
        System.out.println("Distinct strings: " + Distinct());
    }
    
    public int Distinct(){
        ArrayList<String> tempArr = arr;
        
        int total = tempArr.size()-1;
        
        for(int t=0; t<total; t++){
            
            for(int i=0; i<total; i++){    
            
                for(int j=i+1; j<=total; j++){
                
                    if(tempArr.get(i).equals(tempArr.get(j))){
                        tempArr.remove(i);
                        total--;
                    }
                }
            }
        }

        return tempArr.size();
        
    }
    
    public void showList(){
        ListIterator<String> itr = arr.listIterator();
        
        while(itr.hasNext()){
            System.out.println("RESULT: " + itr.next());
        }
    }
}
